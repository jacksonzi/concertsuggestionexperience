﻿// --------------------------------------------------------------------------------
// <copyright file="ConcertSuggestionExperienceTestCase.cs" company="Microsoft">
//   Copyright 2014 (c) Microsoft Corporation.
// </copyright>
// --------------------------------------------------------------------------------

namespace Halsey.Skywalk.ConcertSuggestionExperience.Test
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Halsey.P13N.DataSources;
    using Halsey.P13N.PublicSchemas;
    using Halsey.P13N.Testability.Interfaces;
    using Halsey.P13N.Testability.TestRunner;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using FoursquareQuery = Halsey.P13N.PublicSchemas.FoursquareQuery;

    /// <summary>
    ///   Sample test case for an experience
    /// </summary>
    [TestClass]
    public class ConcertSuggestionExperienceTestCase : ExperienceTestCaseBase
    {
        [DeploymentItem("ConcertSuggestionExperience.Configuration.ini")]
        [TestMethod]
        public void ConcertSuggestionExperienceTestCase_HappyFlow()
        {
            ExperienceTestRunner.RunTestCase(
                personalizedExperience: new ConcertSuggestionExperience(),
                experienceTestCase: new ConcertSuggestionExperienceTestCase(),
                configurationContent: System.IO.File.ReadAllText("ConcertSuggestionExperience.Configuration.ini"));
        }

        public override IExperienceDataGenerator DataGenerator
        {
            get { return new ConcertSuggestionExperienceDataGenerator(); }
        }

        /// <summary>
        /// Validates should execute output is always true
        /// </summary>
        /// <param name="shouldExecute">The output returned from ShouldExecute() in the multiEntity experience</param>
        public override void ValidateShouldExecute(bool shouldExecute)
        {
            Assert.IsTrue(shouldExecute, "Should execute returned false");
        }

        /// <summary>
        /// Validates the query was built correctly
        /// </summary>
        /// <param name="queries">The queries returned from BuildQuery() in the multiEntity experience</param>
        public override void ValidateBuildQuery(IList<Query> queries)
        {
            const double Tolerance = 0.0001;
            const double ConfiguredLat = 47.57394342059334;
            const double ConfiguredLong = -122.1705299473441;

            Assert.AreEqual(1, queries.Count, "Build query should return one query");

            var query = (FoursquareQuery)queries.First();

            // Assert query was built as expected (location values are determined by experience configuration)
            Assert.IsFalse(query.IsOpenNow, "query.IsOpenNow should return false");
            Assert.IsTrue(Math.Abs(query.Latitude - ConfiguredLat) < Tolerance, "Query latitude is incorrect");
            Assert.IsTrue(Math.Abs(query.Longitude - ConfiguredLong) < Tolerance, "Query longitude is incorrect");
            Assert.AreEqual((uint)1000, query.RadiusInMeters, "Query radius is incorrect");
        }

        /// <summary>
        /// Validates the queries were filtered correctly
        /// </summary>
        /// <param name="queryResults">The queries returned from Filter() in the multiEntity experience</param>
        public override void ValidateFilter(IList<QueryResult> queryResults)
        {
            Assert.AreEqual(1, queryResults.Count, "Filter should return one query result");

            var queryResult = queryResults.First();

            Assert.IsNotNull(queryResult.FoursquareQueryResultItems, "Filter returned null FoursquareQueryResultItems");
            Assert.AreEqual(2, queryResult.FoursquareQueryResultItems.Count, "Filter returned wrong number of FoursquareQueryResultItems");

            // Assert only two queries were returned by the filter (venue1 and venue4 are supposed to get filtered)
            Assert.AreEqual("venue2", queryResult.FoursquareQueryResultItems[0].VenueName, "Filter returned wrong query results");
            Assert.AreEqual("venue3", queryResult.FoursquareQueryResultItems[1].VenueName, "Filter returned wrong query results");
        }

        /// <summary>
        /// Validates the queries were ranked correctly
        /// </summary>
        /// <param name="queryResults">The queries returned from Rank() in the multiEntity experience</param>
        public override void ValidateRank(IList<QueryResult> queryResults)
        {
            Assert.AreEqual(1, queryResults.Count, "Ranker should return one query result");

            var queryResult = queryResults.First();

            Assert.IsNotNull(queryResult.FoursquareQueryResultItems, "Ranker returned null FoursquareQueryResultItems");
            Assert.AreEqual(2, queryResult.FoursquareQueryResultItems.Count, "Ranker returned wrong number of FoursquareQueryResultItems");

            Assert.AreEqual("venue3", queryResult.FoursquareQueryResultItems[0].VenueName, "Ranker returned wrong query results");
            Assert.AreEqual("venue2", queryResult.FoursquareQueryResultItems[1].VenueName, "Ranker returned wrong query results");
        }

        /// <summary>
        /// Validates the rendering model was built correctly
        /// </summary>
        /// <param name="dataModel">The data model returned from BuildRenderingModel() in the multiEntity experience</param>
        public override void ValidateBuildRenderingModel(DataModel dataModel)
        {
            Assert.IsNotNull(dataModel, "Builder returned null data model");
            var multiEntityDataModel = (MultiEntityDataModel)dataModel;

            // Assert data model holds two simple entities of venue2 and venue3 in the right order
            Assert.AreEqual(2, multiEntityDataModel.SimpleEntitys.Count, "Builder returned wrong number of entities");

            Assert.AreEqual("venue3", multiEntityDataModel.SimpleEntitys[0].Title, "Builder returned wrong entities");
            Assert.AreEqual("venue2", multiEntityDataModel.SimpleEntitys[1].Title, "Builder returned wrong entities");
        }
    }
}
