// ----------------------------------------------------------------------------------------------
// <copyright file="ConfigurationBase.Interface.Generated.cs" company="Microsoft">
//     This file is generated by a tool.
// Copyright (c) Microsoft Corporation. All rights reserved.
// </copyright>
// ----------------------------------------------------------------------------------------------

//////////////////////////////////////////////////////////////////////////////////////////////////
//// GENERATED CODE - PLEASE DO NOT MODIFY BY HAND!!! ALL YOUR CHANGES WILL BE OVERWRITTEN!!! ////
//////////////////////////////////////////////////////////////////////////////////////////////////

namespace Halsey.P13N
{
    /// <summary>
    /// Undocumented interface 'ConfigurationBase'.
    /// </summary>
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1040", Justification = "Parallax uses this interface to load variant object types for which schema is not yet defined.")]
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("microsoft.search.platform.parallax.tools.codegenerator.exe", "1.0.0.0")]
    public interface ConfigurationBase
    {
    }
}
