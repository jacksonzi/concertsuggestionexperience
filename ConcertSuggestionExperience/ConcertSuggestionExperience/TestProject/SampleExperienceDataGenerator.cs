﻿// ---------------------------------------------------------------------------
// <copyright file="$saferootprojectname$ExperienceDataGenerator.cs" company="Microsoft">
//   Copyright 2014 (c) Microsoft Corporation.
// </copyright>
// ---------------------------------------------------------------------------

namespace Halsey.Skywalk.$experiencename$.Test
{
    using Halsey.P13N;
    using Halsey.P13N.DataSources;
    using Halsey.P13N.PublicSchemas;
    using Halsey.P13N.Testability.Interfaces;
    using Halsey.P13N.Testability.Mocks;
    using Moq;
    using System;
    using System.Collections.Generic;    
    using MockFactory = Halsey.P13N.Testability.Mocks.MockFactory;
    using QueryResult = P13N.PublicSchemas.QueryResult;

    public class $experiencename$DataGenerator : IExperienceDataGenerator
    {
        /// <summary>
        ///   Gets the id associated with data generator data
        /// </summary>
        public string Id
        {
            get
            {
                return "$experiencename$";
            }
        }

        /// <summary>
        ///   Generates the mock user. This user is used and its mocked properties is used either in test case or in real experience serving (based on the ID)
        /// </summary>
        /// <returns>The user</returns>

        public IUser GenerateUser()
        {
            // Set up location
            var user = new Mock<IUser>();
            var currentLocation = MockFactory.CreateLocationMock(latitude: 33, longitude: 22, locationType: LocationType.Other);

            // Far from home
            var place = MockFactory.CreateLocationMock(latitude: 33, longitude: -122, locationType: LocationType.Home);
            place.SetupGet(c => c.Latitude).Returns(33.00);
            place.SetupGet(c => c.Longitude).Returns(-142.00);
            place.SetupGet(c => c.Type).Returns(LocationType.Home);

            user.Setup(c => c.Places).Returns(new[] { place.Object });
            user.Setup(c => c.CurrentLocation).Returns(currentLocation.Object);

            // Set up user interest
            var tracker = new Mock<ITracker>();
            tracker.SetupGet(i => i.Id).Returns("SampleTracker");

            var interest = new Mock<IInterest>();
            interest.SetupGet(i => i.Id).Returns("SampleInterest");
            interest.SetupGet(i => i.Trackers).Returns(new[] { tracker.Object });
            user.SetupGet(c => c.Interests).Returns(new[] { interest.Object });

            // Mock inference for Chinese cuisine type. Other inferences can be set up as well to test different scenarios
            var features = new Mock<IFeatures>();
            features.SetupGet(i => i.CuisineTypes)
                .Returns(new List<CuisineType>()
                {
                    CuisineType.Chinese,
                });

            user.Setup(c => c.Features).Returns(features.Object);

            user.SetupGet(r => r.RequestTime).Returns(DateTime.Now);
            return user.Object;
        }

        public IRequest GenerateRequest()
        {
            var request = new Mock<IRequest>();
            return request.Object;
        }

        /// <summary>  
        /// Generates the list of query results for the mocked data
        /// These query results will be used in the experience methods
        ///   <see cref="PersonalizedExperience.Filter"/>
        ///   <see cref="PersonalizedExperience.Rank"/>
        ///   <see cref="PersonalizedExperience.BuildRenderingModel"/>
        /// </summary>
        /// <returns>List of query results</returns>
        /// <remarks>
        /// This is currently only used in automatic validations during publishing
        /// When using the data generator in simulation this is not used, and instead queries are made to the actual data source
        /// </remarks>
        public IList<QueryResult> GenerateQueryResults()
        {
            var foursquareQueryResultItem1 = MockFactory.CreateFoursquareQueryResultItemMock();
            var foursquareQueryResultItem2 = MockFactory.CreateFoursquareQueryResultItemMock();
            var foursquareQueryResultItem3 = MockFactory.CreateFoursquareQueryResultItemMock();
            var foursquareQueryResultItem4 = MockFactory.CreateFoursquareQueryResultItemMock();

            foursquareQueryResultItem1.VenueName = "venue1";
            foursquareQueryResultItem1.PriceRange = FoursquarePrice.Tier4;
            foursquareQueryResultItem1.Rating = 5;
            foursquareQueryResultItem1.DistanceFromSearchLocation = 15;

            foursquareQueryResultItem2.VenueName = "venue2";
            foursquareQueryResultItem2.PriceRange = FoursquarePrice.Tier3;
            foursquareQueryResultItem2.Rating = 4;
            foursquareQueryResultItem2.DistanceFromSearchLocation = 20;

            foursquareQueryResultItem3.VenueName = "venue3";
            foursquareQueryResultItem3.PriceRange = FoursquarePrice.Tier2;
            foursquareQueryResultItem3.Rating = 3;
            foursquareQueryResultItem3.DistanceFromSearchLocation = 10;

            foursquareQueryResultItem4.VenueName = "venue4";
            foursquareQueryResultItem4.PriceRange = FoursquarePrice.Tier1;
            foursquareQueryResultItem4.Rating = 2;
            foursquareQueryResultItem4.DistanceFromSearchLocation = 50;

            var queryResult = MockFactory.CreateQueryResultMock(
                DataSourceType.Foursquare,
                new List<IQueryResultMockGenerator> { foursquareQueryResultItem1, foursquareQueryResultItem2, foursquareQueryResultItem3, foursquareQueryResultItem4 });

            // Returns the single result created above
            return new[] { queryResult };
        }
    }
}
