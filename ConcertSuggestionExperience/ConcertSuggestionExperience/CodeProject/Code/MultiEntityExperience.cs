﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="$experiencename$.cs" company="Microsoft">
//   Copyright 2014 (c) Microsoft Corporation.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Halsey.Skywalk.$experiencename$
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Halsey.P13N.DataSources;
    using Halsey.P13N.PublicSchemas;

    /// <summary>
    ///   Sample experience for service validation
    /// </summary>
    public class $experiencename$ : PersonalizedExperience
    {
        /// <summary>
        ///   Indicates whether the personalized experience should run
        /// </summary>
        /// <param name="user">Contains data about the user (e.g., User features, current location, client time, etc...)</param>
        /// <param name="request">Contains data about the request</param>
        /// <returns>True if the experience is enabled, false otherwise</returns>
        public override bool ShouldExecute(IUser user, IRequest request)
        {
            // This experience is always enabled.
            // Remove and uncomment below code to experience with execution based on varying conditions
            return true;

            // Example for executing only if the user likes Chinese food:
            /*    
                if (user.Features.CuisineTypes != null)
                {
                   return user.Features.CuisineTypes.Any(t => t == CuisineType.Chinese);
                }
                return false;
             */

            // Example for executing only if user is far away from home (example test case can be found in UserFarFromHomeTestCase)
            /*  
                var configuration = this.Utilities.GetConfiguration<Configuration>();
                Distance distanceFromHome;
                ILocation homeLocation = user.Places.FirstOrDefault(p => p.Type == LocationType.Home);
                if (homeLocation != null &&
                    user.CurrentLocation != null && 
                    user.CurrentLocation.TryGetDistance(homeLocation, out distanceFromHome) &&
                    configuration.MinDistanceFromHomeToExecute < distanceFromHome.ValueInKm * 1000)
                {
                    return true;
                }
                return false;
             */

            // Example for executing only if the user has a recommendation.foursquare interest with a tracker turned on:
            /*
                return user.Interests.Any(
                   i => i.CategoryId == "recommendation.foursquare" && i.Trackers.Any(t => t.IsOn));
             */
        }

        /// <summary>
        ///   Builds a single personal query, based on user data
        /// </summary>
        /// <param name="user">Contains data about the user (e.g., User features, current location, client time, etc...)</param>
        /// <param name="request">Contains data about the request</param>
        /// <param name="queryFactory">Existing query builders for generating the personal query</param>
        /// <returns>The concrete query to a single data source</returns>
        public override IList<Query> BuildQuery(IUser user, IRequest request, IQueryFactory queryFactory)
        {
            // Use the location in the configuration to build the query
            var configuration = this.Utilities.GetConfiguration<Configuration>();
            var query = queryFactory.BuildFoursquareQuery(configuration.Latitude, configuration.Longitude);

            // Use current user location instead
            /*
            var query = queryFactory.BuildFoursquareQuery(user.CurrentLocation);            
            */

            query.RadiusInMeters = 1000;
            query.ResultLimit = 10;


            // Examples of more specific queries:
            /*    
            query.IsOpenNow = true;
            query.Section = FoursquareSection.Arts;
            query.Section = FoursquareSection.Coffee;
            query.Section = FoursquareSection.Drinks;
            query.Section = FoursquareSection.Food;
            query.Section = FoursquareSection.Shops;
            query.Section = FoursquareSection.Sights;
             */

            return new List<Query> { query };
        }

        /// <summary>
        ///   Filter entities using Linq queries or dedicated filter
        /// </summary>
        /// <param name="user">Contains data about the user (e.g., User features, current location, client time, etc...)</param>
        /// <param name="request">Contains data about the request</param>
        /// <param name="queryResults">The result of running the query for the specified data source</param>
        /// <param name="filterFactory">Container for all filters</param>
        /// <returns>The filtered result set</returns>
        public override IList<QueryResult> Filter(IUser user, IRequest request, IList<QueryResult> queryResults, IFilterFactory filterFactory)
        {
            var queryResult = queryResults.FirstOrDefault();

            if (queryResult == null || queryResult.FoursquareQueryResultItems == null || !queryResult.FoursquareQueryResultItems.Any())
            {
                this.Logger.Info("Not filtering since there are no results");
                return queryResults;
            }

            // Filter venues with low ratings or with the most expensive price tier.
            queryResult.FoursquareQueryResultItems =
                queryResult.FoursquareQueryResultItems.Where(i => i.Rating >= 3 && i.PriceRange != FoursquarePrice.Tier4).ToList();

            return queryResults;

            // Filter items that were seen by the user more than 10 times in the past 5 days
            /*
            var freshnessFilter = filterFactory.CreateFreshnessFilter();
                return freshnessFilter.Filter(
                    new TimeIntervalCapFreshnessPolicy(TimeSpan.FromDays(5), cap: 10), queryResults);
             */
        }

        /// <summary>
        ///   Rank items using Linq queries or dedicated <paramref name="rankerFactory"/>
        /// </summary>
        /// <param name="user">Contains data about the user (e.g., User features, current location, client time, etc...)</param>
        /// <param name="request">Contains data about the request</param>
        /// <param name="queryResults">The result of running the query for the specified data source (after filtering)</param>
        /// <param name="rankerFactory">Container for all rankers</param>        
        /// <returns>The ranked result set</returns>
        public override IList<QueryResult> Rank(IUser user, IRequest request, IList<QueryResult> queryResults, IRankerFactory rankerFactory)
        {
            var queryResult = queryResults.FirstOrDefault();
            if (queryResult == null || queryResult.FoursquareQueryResultItems == null || !queryResult.FoursquareQueryResultItems.Any())
            {
                this.Logger.Info("Not ranking since there are no results");
                return queryResults;
            }

            // Rank venues - closest first
            queryResult.FoursquareQueryResultItems = queryResult.FoursquareQueryResultItems.OrderBy(v => v.DistanceFromSearchLocation).ToList();

            return queryResults;
        }

        /// <summary>
        ///   Builds the UX rendering model object that defines which template will be used, and customizes it.
        /// </summary>
        /// <param name="user">Contains data about the user (e.g., User features, current location, client time, etc...)</param>
        /// <param name="request">Contains data about the request</param>
        /// <param name="queryResults">The result of running the query for the specified data source (after filtering and ranking)</param>
        /// <param name="dataModelFactory">Factory for creating a data model</param>
        /// <returns>The UX rendering model</returns>
        public override DataModel BuildRenderingModel(
            IUser user,
            IRequest request,
            IList<QueryResult> queryResults,
            IDataModelFactory dataModelFactory)
        {
            this.Logger.Info("Building a rendering model for MultiEntityExperience");

            var configuration = this.Utilities.GetConfiguration<Configuration>();

            // The untrack action in the card will disable the following interest.
            IInterest interest =
                user.Interests.FirstOrDefault(i => i.CategoryId == "recommendation.foursquare");


            // To enable running the sample experience without modifying the code, 
            // the call uses the first interest in the UX model factory.
            // Real experience should check condition in the ShouldExecute() method and
            // return true only if the user has the specific interest
            if (interest == null)
            {
                interest = user.Interests.First(i => i.Trackers.Any());
            }

            ITracker tracker = interest.Trackers.First();

            var untrackAction = this.ActionFactory.CreateUntrackAction(interest.Id, tracker.Id, configuration.UntrackActionText);

            var dataModel = dataModelFactory.CreateMultiEntityDataModel(
                title: configuration.Title,
                queryResult: queryResults.First(),
                actions: new List<ActionBase>() { untrackAction });

            // Override an item in the card to show different values than the default values:
            /*
            dataModel.SimpleEntitys[0].Title = "My Generic Title";
            dataModel.SimpleEntitys[0].TextLines.Add("Desctiption line1");
            dataModel.SimpleEntitys[0].TextLines.Add("Desctiption line2");
            dataModel.SimpleEntitys[0].Link = "bing://maps";
             */

            return dataModel;
        }
    }
}